--Mario NN written by Michael Orzel Fall 2017
--Sources cited on project report and in comments
--[[
------Source A------
structure influenced by Michael Roberts 
06/2015
https://pastebin.com/0RJrwspT 

------Source B------
youtube series by Jeff Heaton
https://www.youtube.com/watch?v=ujBiM9stPHU (Part 1 through part 5)
--]]
--Variables for main
saveState = savestate.object(1)
savestate.save(saveState)

timerHold = 0
lastScreen = 0
screenTimeStuck = 0
maxScreenStuckTime = 200
playerX = 0
playerY = 0
generation = 1

--View variables
viewRadius = 2
viewDiameter = 2*viewRadius+1
playerRoomX = 0
playerRoomY = 0
playerMapX = 0
playerMapY = 0
playerLives = 0
MapTileNum = 208
map = {}
for i=1,2*MapTileNum do
  map[i] = 0
end
inputNum = viewDiameter*viewDiameter + 1
viewBox = {}
for i=1,viewDiameter*viewDiameter do
  viewBox[i] = 0
end
--enemy variables
enemyX = {}
enemyY = {}
enemyMapX = {}
enemyMapY = {}
EnemyNumSlots = 5

--NN variables
currentChild = 1
currentParent1 = 0
outputStepStrength = 0 
currentParent2 = 0
mutationProbability = .1
outputNum = 6 --6 buttons on the controller
layerNum = 2
layerSize = {inputNum,  6, outputNum}
populationSize = 12
populationFitness = {}
for i=1, populationSize do
  populationFitness[i] = nil
end
Y = {}  --array for buttons
for i=1, outputNum do
  Y[i] = false
end
layer = {}  --array for layer
tempLayer = {}  --array also for temp layer
delta = {}
for l=1,layerNum+1 do
  layer[l] = {}
  tempLayer[l] = {}
  delta[l] = {}
  for i=1, layerSize[l] do
    layer[l][i] = 0
    tempLayer[l][i] = 0
    delta[l][i] = 0
  end
end


gene = {} --array of genes
memGene = {}
for p=1, populationSize do
  gene[p] = {}
  for l=1,layerNum do
    gene[p][l] = {}
    memGene[l] = {}
    for i=1,layerSize[l+1] do
      gene[p][l][i] = {}
      memGene[l][i] = {}
      for j=1,layerSize[l] do
        gene[p][l][i][j] = 2*math.random() 
        memGene[l][i][j] = gene[p][l][i][j]
      end
    end
  end
end
----------------------------------------------------------
--initialization functions
function updateScreen() --loads everything on-screen stuff, modified version of source [A]
  --enemies locations
  for i=1,EnemyNumSlots do
    if memory.readbyte(0xF+(i-1)) ~= 0 then
      enemyX[i] = memory.readbyte(0x87+(i-1)) + memory.readbyte(0x6E+(i-1))*0x100
      enemyY[i] = memory.readbyte(0xCF+(i-1)) + 24
    else
      enemyX[i] = -1
      enemyY[i] = -1
    end
    enemyMapX[i] = math.floor((enemyX[i]%512)/16)+1
    enemyMapY[i] = math.floor((enemyY[i]-32)/16)
  end
  --map
  for i=1, 2*MapTileNum do
    if memory.readbyte(0x500 + i-1) ~= 0 then
      map[i] = 1
    else
      map[i] = 0
    end
  end
  
  for i=1, EnemyNumSlots do
    if memory.readbyte(0xF+(i-1)) ~= 0 then
      local temp2 = math.floor(enemyMapX[i]/16)
      local xAddress = enemyMapX[i] - 16*temp2
      local yAddress = enemyMapY[i] - 1 + 13*(temp2%2)
      if xAddress >= 1 and xAddress < 32 and yAddress >= 1 and yAddress <= 25 then
        map[xAddress + 16*yAddress] = -1
      end
    end
  end
  
  --mario's view
  for i = -viewRadius, viewRadius do
    for j = -viewRadius, viewRadius do
    local x = playerMapX+i-1
    local y = playerMapY+j-1
    local temp1 = math.floor(x/16)
    local xAddress = x - 16*temp1+1
    local yAddress = y + 13*(temp1%2)
      if xAddress >= 1 and xAddress < 32 and yAddress >= 1 and yAddress <= 25 then
        viewBox[(i+viewRadius+1)+viewDiameter*(j+viewRadius)] = map[xAddress + 16*yAddress]
      else
        viewBox[(i+viewRadius+1)+viewDiameter*(j+viewRadius)] = 0
      end
    end
  end
end

function updateMario() --loads mario's attributes
  playerX = memory.readbyte(0x86) + memory.readbyte(0x6D)*0x100 + 4
  playerY = memory.readbyte(0x3B8) + 16
  playerRoomX = math.floor(playerX/8)+1
  playerRoomY = math.floor(playerY/7.5)-7
  playerMapX = math.floor((playerX%512)/16)+1
  playerMapY = math.floor((playerY-32)/16)+1
end

function inputViewToX(startPosition) --field of view for mario
  for i=1,(viewDiameter)*(viewDiameter) do
    layer[1][startPosition-1+i] = viewBox[i]
  end
end

-- math functions, basic math functions also in source [A]
function minimum(vectorIn)
  local val = vectorIn[1]
  local index = 1
  for i=1, #vectorIn do
    if vectorIn[i] < val then
      index = i
      val = vectorIn[i]
    end
  end
  return index
end

function maximum(vectorIn)
  local val = vectorIn[1]
  local index = 1
  for i=1, #vectorIn do
    if vectorIn[i] > val then
      index = i
      val = vectorIn[i]
    end
  end
  return index
end

function multiply(matrixIn, vectorIn, rows, collumns)
  local vectorOut = {}
  for i=1,rows do
      vectorOut[i] = 0
      for j=1,collumns do
        vectorOut[i] = vectorOut[i] + matrixIn[i][j]*vectorIn[j]
      end
  end
  
  return vectorOut
end

function sigmoid(inputVector, inputLength, strength)
  local vectorOut = {}
  for i=1,inputLength do
    vectorOut[i] = 2/(1+math.exp(-strength*inputVector[i])) - 1
  end
  return vectorOut
end

--network functions, modified version of source [A,B]
function stepFunction(inputVector, inputLength)
  local vectorOut = {}
  for i=1,inputLength do
    if inputVector[i] >= 0 then
      vectorOut[i] = true
    else
      vectorOut[i] = false
    end
  end
  return vectorOut
end

function forwardPropogate() --multiplies then sigmoid
    layer[1][1] = -1
  for l=1, layerNum do
    tempLayer[l+1] = multiply(gene[currentChild][l], layer[l], layerSize[l+1], layerSize[l])
    layer[l+1] = sigmoid(tempLayer[l+1], layerSize[l+1],2)
    if l < layerNum then
      layer[l+1][1] = -1
    end
  end

  Y = stepFunction(layer[layerNum+1], outputNum)

  --make it so up/down etc. can't be hit at same time
  badInputs(3,5) -- left/right
  badInputs(2,6) -- up/down
  badInputs(5,6) -- right/down
  badInputs(3,6) -- left/down
end

function badInputs(button1, button2) --prevents inputs that would conflict with eachother ex: left and right
  if Y[button1] == true and Y[button2] == true then
    if layer[layerNum+1][button1] > layer[layerNum+1][button2] then
      Y[button2] = false
    else
      Y[button1] = false
    end
  end
end

function controllerOutput()
  local controllerInput = {}
  controllerInput = {A=Y[1], up=Y[2], left=Y[3], B=Y[4], right=Y[5], down=Y[6]}
  joypad.write(1, controllerInput)
end

function restart()
  generation = generation + 1/populationSize
  if populationFitness[currentChild] == nil then
    populationFitness[currentChild] = fitness
  elseif fitness > populationFitness[currentChild] then
    populationFitness[currentChild] = fitness
  end

  if #populationFitness < populationSize then
    currentChild = currentChild + 1
  else
    currentChild = minimum(populationFitness)
 
    currentParent1 = maximum(populationFitness)
  
    if currentChild == currentParent1 then
      currentChild = currentChild+1
    end
      
    currentParent2 = math.random(populationSize-2)
    if currentParent2 >= currentChild then
      currentParent2 = currentParent2+1
    end
    if currentParent2 >= currentParent1 then
      currentParent2 = currentParent2+1
    end
    if currentParent2 == currentChild then
      currentParent2 = currentParent2+1
    end
    
    uniformCrossover(mutationProbability)
  end

  screenTimeStuck = 0
  savestate.load(saveState)
  updateMario()
  startFitness = playerX
end

function uniformCrossover(probability) -- if random()< .5 -> take from parent a, else, take from parent b
  for l=1, layerNum do
    for i=1, layerSize[l+1] do
    for j=1, layerSize[l] do
      if math.random() < probability then
        gene[currentChild][l][i][j] = 2*math.random()-1
      else
        if math.random() <=0.5 then
          gene[currentChild][l][i][j] = gene[currentParent1][l][i][j]
        else
          gene[currentChild][l][i][j] = gene[currentParent2][l][i][j]
        end
      end
    end
    end
  end
end

--Main--
updateMario()
lastScreen = memory.readbyte(0x6D)
startFitness = playerX
playerLives = memory.readbyte(0x75A)

while(true)do
  updateMario()
  updateScreen()
  inputViewToX(1)
  forwardPropogate()
  controllerOutput()
  fitness = playerX - startFitness

  if memory.readbyte(0x6D) == lastScreen then
    screenTimeStuck = screenTimeStuck + 1
  else
    screenTimeStuck = 0
  end

  if (screenTimeStuck >= maxScreenStuckTime) then
    emu.print(fitness)
    restart()
  end

  if currentParent1 > 0 then
    gui.box(4, 8*currentParent1, 10, 8*(currentParent1+1)-2, "red")
  end
  if currentParent2 > 0 then
    gui.box(4, 8*currentParent2, 10, 8*(currentParent2+1)-2, "blue")
  end
  gui.box(4, 8*currentChild, 10, 8*(currentChild+1)-2, "green")
  for i=1, populationSize do
    if populationFitness[i] ~= nil then
      gui.text(12, 8*i, populationFitness[i])
    end
  end

  gui.text(220, 8, math.floor(generation))
  gui.text(48, 8, fitness)

  lastScreen = memory.readbyte(0x6D)
  
  emu.frameadvance()
end